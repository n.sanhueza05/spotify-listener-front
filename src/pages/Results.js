import axios from 'axios';
import { useState, useEffect } from 'react';
import { Col, Container, Row,Card } from 'react-bootstrap';
import CargandoDatos from '../components/CargandoDatos';
import CardArtistas from '../components/CardArtistas';
import CardTopCancion from '../components/CardTopCanciones';
import ChartGeneros from '../components/ChartGeneros';
import CarouselRecomendacion from '../components/CarouselRecomendacion';


function ResultsPage() {
  const [responseObject, setResponseObject] = useState({});
  const [isLoading,setIsLoading] = useState(true)
  const auth = localStorage.getItem('refresh_token')

  useEffect(() => {
    const footer = document.querySelector('.footer');
    footer.style.display = "none";

    if (!auth) {
      // Si auth no tiene valor, no se hace la solicitud
      return;
    }

    async function getDataUser(){
      let url = process.env.REACT_APP_URL_API + "api/get_user_data"
      await axios.get(url,{
        headers:{
          'Authorization': "Bearer " + auth
        }
      }).then(r => {
        setResponseObject(r.data)
        setIsLoading(false)
        console.log(r.data)
        footer.style.display = "block";
      }).catch( e =>{
        console.log(e)
      })
    }

    getDataUser()

    

  }, [auth]);

  return (
    <>
      {isLoading ? <CargandoDatos/> : 
        <Container fluid id='bg-results'>
          <Row className='justify-content-center'>
            <Col xs={10} md={8} className='animate__animated animate__fadeInDown mt-md-5'>
              <h1 className='text-center mt-5 fw-bold'>Aquí está toda tu información de gustos musicales de Spotify de los últimos 6 meses.<br/> ¡Disfrútala!</h1>
            </Col>
          </Row>
          { responseObject.current_playing.artist_name.length === 0  ?  null  :
            <Row className='justify-content-center mt-5'>
              <Col xs={10} md={6} lg={3}>
                  <Card className='animate__animated animate__fadeInDown' bg='dark'>
                    <Card.Img variant="top" src={responseObject.current_playing.image} />
                  <Card.Body className='text-opacity-75'>
                    <Card.Text>
                      {responseObject.current_playing.is_playing ? <span>Actualmente te encuentras escuchando</span> : <span>Tu última canción fue</span>} 
                      <span className='fw-bold'><br/>{responseObject.current_playing.song_name}<br/></span>
                      <span className='fw-bold'>De {responseObject.current_playing.artist_name}</span>
                    </Card.Text>
                  </Card.Body>
                </Card>
              </Col>
            </Row>
          }
          <Row className='justify-content-center'>
            <Col xs={10} className='animate__animated animate__fadeInDown'>
              <h1 className='text-center mt-5 fw-bold'>Tu Top de 5 Artistas escuchados!</h1>
            </Col>
          </Row>
          <Row className='justify-content-center mt-5'>
            <Col xs={10} md={6} lg={3}>
              <CardArtistas artist={responseObject.top_5_artists[0]} number={1}/>
            </Col>
          </Row>
          <Row className='justify-content-center mt-5'>
            <Col xs={10} md={6} lg={3}>
              <CardArtistas artist={responseObject.top_5_artists[1]} number={2}/>
            </Col>
            <Col xs={10} md={6} lg={3} className="mt-5 mt-md-0">
              <CardArtistas artist={responseObject.top_5_artists[2]} number={3}/>
            </Col>
          </Row>
          <Row className='justify-content-center mt-5'>
            <Col xs={10} md={6} lg={3}>
              <CardArtistas artist={responseObject.top_5_artists[3]} number={4}/>
            </Col>
            <Col xs={10} md={6} lg={3} className="mt-5 mt-md-0" >
              <CardArtistas artist={responseObject.top_5_artists[4]} number={5}/>
            </Col>
          </Row>
          <Row className='justify-content-center'>
            <Col xs={10}>
              <h1 className='text-center mt-5 fw-bold'>Tu Top de 5 Canciones escuchadas!</h1>
            </Col>
          </Row>
          <Row className='justify-content-center mt-5'>
            <Col xs={10} md={6} lg={3}>
              <CardTopCancion song={responseObject.top_5_songs.items[0]} number={1}/>
            </Col>
          </Row>
          <Row className='justify-content-center mt-5'>
            <Col xs={10} md={6} lg={3}>
              <CardTopCancion song={responseObject.top_5_songs.items[1]} number={2}/>
            </Col>
            <Col xs={10} md={6} lg={3} className="mt-5 mt-md-0">
              <CardTopCancion song={responseObject.top_5_songs.items[2]} number={3}/>
            </Col>
          </Row>
          <Row className='justify-content-center mt-5'>
            <Col xs={10} md={6} lg={3}>
              <CardTopCancion song={responseObject.top_5_songs.items[3]} number={4}/>
            </Col>
            <Col xs={10} md={6} lg={3} className="mt-5 mt-md-0">
              <CardTopCancion song={responseObject.top_5_songs.items[4]} number={5}/>
            </Col>
          </Row>
          <Row className='justify-content-center'>
            <Col xs={10}>
              <h1 className='text-center mt-5 fw-bold'>Revisemos cómo se distribuyen tus géneros musicales</h1>
            </Col>
          </Row>
          <Row className='justify-content-center'>
            <Col xs={10} md={6} lg={5}>
              <ChartGeneros top5genres={responseObject.top_5_genres}/>
            </Col>
          </Row>
          <Row className='justify-content-center mb-5'>
            <Col xs={10}>
              <h1 className='text-center fw-bold'>Acá te dejamos algunas recomendaciones que te podrían gustar</h1>
            </Col>
          </Row>
          <Row className='justify-content-center pb-5'>
            <Col xs={12} md={8}>
              <CarouselRecomendacion recommends={responseObject.recommended_songs}/>
              
            </Col>
          </Row>
        </Container>
      }
    </>
  );
}

export default ResultsPage;
