import Title from '../components/title'
import Login from '../components/Login';
import {MasEscuchadas} from '../components/MasEscuchadas'
import AcercaDe from '../components/AcercaDe';

function Home() {
  return (
    <>
      <Title />
      <MasEscuchadas />
      <Login/>
      <AcercaDe/>
    </>
  );
}

export default Home;