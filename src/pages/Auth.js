import { useLocation, useNavigate } from "react-router-dom";
import { useEffect } from 'react';
import axios from 'axios'

function Auth() {
    const navigate = useNavigate();
    const location = useLocation();

    useEffect(() => {
        const urlParams = new URLSearchParams(location.search);
        const code = urlParams.get("code");
        const url = process.env.REACT_APP_URL_API + "api/get_refresh_token/" + code;
      
        async function getData() {
          try {
            const response = await axios.get(url);
            const token = response.data;
      
            localStorage.clear();
            localStorage.setItem("refresh_token", token);
      
            navigate('/results', { replace: true });
          } catch (error) {
            console.log('error:', error);
            navigate('/', { replace: true });
          }
        }
      
        getData();
      }, [location, navigate]);

    return null;
}

export default Auth;
