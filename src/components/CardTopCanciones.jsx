import { Card } from 'react-bootstrap';


const CardTopCancion = ({song,number}) =>{
    return(
        <>
            <Card className='animate__animated animate__fadeInDown' bg='dark'>
                <Card.Img variant="top" src={song.album.images[0].url} />
                <Card.Body className='text-opacity-75'>
                  <Card.Text className='text-center'>
                    <span className='fw-semibold fs-5'>Puesto Número {number}<br/></span>
                    <span className='fw-bold fs-3'>{song.name}<br/></span>
                    <span className='fw-bold fs-3'>De {song.artists[0].name}<br/></span>
                  </Card.Text>
                </Card.Body>
            </Card>
        </>
    )
}

export default CardTopCancion