import React, { useEffect } from 'react';
import * as echarts from 'echarts';

const PieChart = ({top5genres}) => {

  useEffect(() => {
    const chart = echarts.init(document.getElementById('pie-chart'));
    if (top5genres.length > 0){
        const options = {
            tooltip: {
              show:false
            },
            legend: {
              top: '5%',
              left: 'center',
              textStyle:{
                color:'white',
                fontSize:20,
              }
            },
            series: [
              {
                name: 'Generos Musicales',
                type: 'pie',
                radius: ['40%', '70%'],
                avoidLabelOverlap: false,
                label: {
                  show: false,
                  position: 'center',
                  color:'white'
                },
                emphasis: {
                  label: {
                    show: true,
                    fontSize: 30,
                    fontWeight: 'bold'
                  }
                },
                labelLine: {
                  show: false
                },
                data: [
                  { value: top5genres[0].count, name: top5genres[0].genre },
                  { value: top5genres[1].count, name: top5genres[1].genre },
                  { value: top5genres[2].count, name: top5genres[2].genre },
                  { value: top5genres[3].count, name: top5genres[3].genre },
                  { value: top5genres[4].count, name: top5genres[4].genre }
                ]
              }
            ]
          };
        chart.setOption(options);
    }
  }, [top5genres]);

  return <div id="pie-chart" style={{ width: '100%', height: 700 }}></div>;
};

export default PieChart;
