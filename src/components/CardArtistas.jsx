import { Card } from 'react-bootstrap';


const CardArtistas = ({artist,number}) =>{
    return(
        <>
            <Card className='animate__animated animate__fadeInDown' bg='dark'>
                <Card.Img variant="top" src={artist.image} />
                <Card.Body className='text-opacity-75'>
                  <Card.Text className='text-center'>
                    <span className='fw-semibold fs-5'>Puesto Número {number}<br/></span>
                    <span className='fw-bold fs-3'>{artist.name}</span>
                  </Card.Text>
                </Card.Body>
            </Card>
        </>
    )
}

export default CardArtistas