import { Col, Container, Row } from "react-bootstrap"
import '../assets/styles/main.css'

const Footer = () =>{
    return(
        <Container fluid>
            <Row className="justify-content-center">
                <Col xs={12} className="text-center text-white footer py-5">
                    <p>Desarrollado por Nicolas Sanhueza - Estudiante Ingenieria Informática - UFRO <br/>
                    2023

                    </p>
                </Col>
            </Row>
        </Container>
    )
}
export default Footer