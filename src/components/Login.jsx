import Button from 'react-bootstrap/Button'
import React from 'react'
import '../assets/styles/custom-button.css'
import { Col, Container, Row } from 'react-bootstrap'

const Login = () => {
    return(
        <>
            <Container fluid>
                <Row className='justify-content-center mt-5'>
                    <Col xs={12} className='text-center'>
                        <Button size='lg' id='spotify-login' className='text-uppercase' onClick={openLogin}>Iniciar Sesión</Button>
                    </Col>
                </Row>
            </Container>
        </>
    )
}

const openLogin = () =>{
    window.location = process.env.REACT_APP_URL_LOGIN;
}

export default Login