import { Container,Row,Col,Spinner } from "react-bootstrap"
import '../assets/styles/loading.css'

const CargandoDatos = () => {
    return(
        <>
            <Container fluid className="center-container">
                <Row>
                    <Col xs={12} className='text-center'>
                        <Spinner animation="grow" variant="primary" />
                        <Spinner animation="grow" variant="secondary" />
                        <Spinner animation="grow" variant="success" />
                        <Spinner animation="grow" variant="danger" />
                        <Spinner animation="grow" variant="warning" />
                        <Spinner animation="grow" variant="info" />
                        <Spinner animation="grow" variant="light" />
                        <Spinner animation="grow" variant="dark" />
                    </Col>
                </Row>
            </Container>
        </>
    )
}

export default CargandoDatos