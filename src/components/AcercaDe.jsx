import { Col, Container, Row } from "react-bootstrap"

const AcercaDe = () => {
    return(
        <Container className="mt-5">
            <Row className="justify-content-center">
                <Col xs={10}>
                    <br/><br/><br/><br/><br/><br/><br/>
                    <div className="fs-3 fw-semibold text-white text-center">Acerca de</div>
                </Col>
            </Row>
            <Row className="justify-content-center mt-5">
                <Col xs={10}>
                    <p className="fs-4 text-center">
                    El objetivo de esta página web es permitir a los usuarios analizar sus gustos musicales utilizando la API de Spotify.
                    Esta herramienta fue desarrollada utilizando React y React Bootstrap como parte del proceso de aprendizaje de estas
                    tecnologías.<br/><br/>
                    Con esta página, los usuarios pueden obtener información detallada sobre sus artistas y canciones favoritas, descubrir
                    nuevos géneros y artistas que podrían interesarles además de obtener estadísticas sobre sus hábitos de escucha.
                    <br/><br/>
                    Para utilizar la página, es necesario iniciar sesión con una cuenta de Spotify y autorizar el acceso a la información musical.<br/><br/>
                    Puedes encontrar más información al respecto en los repositorios del Proyecto <br/> <br/>
                    FrontEnd: <a href="https://gitlab.com/n.sanhueza05/spotify-listener-front">Acceso</a> <br/> 
                    Backend: <a href="https://gitlab.com/n.sanhueza05/spotify-listener-backend">Acceso</a> <br/><br/>
                    </p>
                </Col>
            </Row>
        </Container>
    )
}

export default AcercaDe