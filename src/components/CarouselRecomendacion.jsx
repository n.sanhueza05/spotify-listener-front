import Carousel from 'react-bootstrap/Carousel';
import ReactAudioPlayer from 'react-audio-player';
import { useState } from 'react';

const CarouselRecomendacion = ({ recommends }) => {
  const tracks = recommends.tracks;
  const [selectedItem, setSelectedItem] = useState(0);

  const handleSlide = (index) => {
    const prevItem = selectedItem;
    setSelectedItem(index);
    if (prevItem !== index) {
      const audio = document.getElementById(`audio-${prevItem}`);
      if (audio) {
        audio.pause();
      }
    }
  };

  return (
    <Carousel
      style={{ backgroundColor: 'transparent' }}
      indicators={false}
      interval={null}
      onSlide={handleSlide}
    >
      {tracks.map((item, index) => (
        <Carousel.Item
          key={item.name}
          className="text-center"
          style={{ backgroundColor: 'transparent' }}
        >
          <img src={item.album.images[0].url} alt={item.name} width={300} height={300} />
          <Carousel.Caption style={{ backgroundColor: 'rgba(0,0,0,0.4)' }}>
            <h3 style={{ backgroundColor: 'transparent' }} className="fs-3 fw-bold">
              {item.name}
            </h3>
            <p style={{ backgroundColor: 'transparent' }} className="fs-5">
              Album: {item.album.name}
            </p>
            <p style={{ backgroundColor: 'transparent' }} className="fs-5">
              De: {item.artists[0].name}
            </p>
            <ReactAudioPlayer
              id={`audio-${index}`}
              volume={0.1}
              src={item.preview_url}
              controls
            />
          </Carousel.Caption>
        </Carousel.Item>
      ))}
    </Carousel>
  );
};

export default CarouselRecomendacion;
