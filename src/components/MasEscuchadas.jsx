import React,{useState,useEffect} from 'react'
import '../assets/styles/container-albums.css'
import { Spinner } from 'react-bootstrap';


const getData = () => {
  let url = process.env.REACT_APP_URL_API + "api/get_top_10";
  return fetch(url).then(response => {
    if (!response.ok) {
      throw new Error('Error al cargar los datos.');
    }
    return response.json();
  });
}

export const MasEscuchadas = () => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    let isMounted = true;
    getData().then(data => {
      if (isMounted) {
        setItems(data.items);
        setLoading(false);
      }
    }).catch(error => {
      console.log(error);
      setLoading(false);
    });

    return () => { 
      isMounted = false
      
    };
  }, []);

  return (
    <div className='mt-5 text-center'>
      {loading ? <div className='py-5 my-5'>
      <Spinner className='mx-auto' animation="grow" variant="light" />
      <Spinner animation="grow" variant="warning" />
      <Spinner animation="grow" variant="danger" />
      </div> : <Banner items={items} />}

    </div>
  );
}


const Banner = ({ items, speed = 100000 }) => {
  console.log(items)
  return (
    <div className="inner">
      <div className="wrapper">
        <section style={{ "--speed": `${speed}ms` }}>
          {items.map((item) => (
            <div className="image" key={item.track.album.name}>
              <img src={item.track.album.images[1].url} alt={'album foto'} />
              <div className='text-data-song fw-bold'>{item.track.album.name}</div>
              <div className='text-data-song'>{item.track.album.artists[0].name}</div>
            </div>
          ))}
        </section>
        <section style={{ "--speed": `${speed}ms` }}>
          {items.map((item) => (
            <div className="image" key={item.track.album.name}>
              <img src={item.track.album.images[1].url} alt={'album foto'} />
              <div className='text-data-song fw-bold'>{item.track.album.name}</div>
              <div className='text-data-song'>{item.track.album.artists[0].name}</div>
            </div>
          ))}
        </section>
        <section style={{ "--speed": `${speed}ms` }}>
          {items.map((item) => (
            <div className="image" key={item.track.album.name}>
              <img src={item.track.album.images[1].url} alt={'album foto'} />
              <div className='text-data-song fw-bold'>{item.track.album.name}</div>
              <div className='text-data-song'>{item.track.album.artists[0].name}</div>
            </div>
          ))}
        </section>
      </div>
    </div>
  );
};

export { Banner };