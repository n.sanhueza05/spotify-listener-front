import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Image from 'react-bootstrap/Image'
import logo from '../assets/images/Spotify_Logo.png'

function Title() {
  return (
    <Container fluid className='pt-5'>
      <Row>
        <Col xs={12} className='text-end animate__animated animate__fadeInDown '>
          <Image src={logo} width='150' style={{"objectFit": `contain`}} className='me-5 mb-md-5 mb-3'></Image>
        </Col>
      </Row>
      <Row className='justify-content-center'>
        <Col className='animate__animated animate__fadeInDown' lg={12} xs={10}>
          <h1 className='text-center fs-1 fw-bold'>Descubre tu contenido más escuchado en Spotify!</h1>
        </Col>
      </Row>
    </Container>
  );
}

export default Title;