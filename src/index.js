import React from 'react';
import ReactDOM from 'react-dom/client';
import Home from './pages/home'
import Layout from './pages/layout'
import { BrowserRouter, Routes, Route } from "react-router-dom";
import AuthPage from './pages/Auth'
import ResultsPage from './pages/Results';
import './assets/styles/custom.scss'
import './assets/styles/main.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'animate.css';


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <BrowserRouter>
    <Routes>
      <Route path="/" element={<Layout />}>
        <Route index element={<Home />} />
        <Route path="/auth" element={<AuthPage/>}/>
        <Route path="/results" element={<ResultsPage/>}/>
        <Route path="*" element={<Home />}/>
      </Route>
    </Routes>
  </BrowserRouter>
);

